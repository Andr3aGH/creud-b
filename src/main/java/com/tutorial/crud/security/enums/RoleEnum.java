package com.tutorial.crud.security.enums;

public enum RoleEnum {
    ROLE_ADMIN, ROLE_USER
}
