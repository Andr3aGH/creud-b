package com.tutorial.crud.security.repository;

import com.tutorial.crud.security.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, Integer> {
        Optional<UserEntity> findByUserName(String username);
        boolean existsByUserName (String username);
        boolean existsByEmail(String email);
}
