package com.tutorial.crud.security.repository;

import com.tutorial.crud.security.entity.RoleEntity;
import com.tutorial.crud.security.enums.RoleEnum;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<RoleEntity, Integer> {
    Optional<RoleEntity> findByRoleEnum(RoleEnum roleEnum);

}
