package com.tutorial.crud.security.entity;

import com.tutorial.crud.security.enums.RoleEnum;
import jakarta.persistence.*;


import javax.validation.constraints.NotNull;

@Entity
public class RoleEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @NotNull
    @Enumerated(EnumType.STRING)
    private RoleEnum roleEnum;

    public RoleEntity() {
    }

    public RoleEntity(@NotNull RoleEnum roleEnum) {
        this.roleEnum = roleEnum;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public RoleEnum getRoleEnum() {
        return roleEnum;
    }

    public void setRoleEnum(RoleEnum roleEnum) {
        this.roleEnum = roleEnum;
    }
}
