package com.tutorial.crud.security.service;

import com.tutorial.crud.security.entity.RoleEntity;
import com.tutorial.crud.security.enums.RoleEnum;
import com.tutorial.crud.security.repository.RoleRepository;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
@Service
@Transactional
public class RoleService {
    @Autowired
    RoleRepository roleRepository;

    public Optional<RoleEntity> getByRoleEnum(RoleEnum roleEnum){
        return roleRepository.findByRoleEnum(roleEnum);
    }
}
