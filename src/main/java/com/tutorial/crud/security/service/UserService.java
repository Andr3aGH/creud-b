package com.tutorial.crud.security.service;

import com.tutorial.crud.security.entity.UserEntity;
import com.tutorial.crud.security.repository.UserRepository;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

//La transactional es para mantener la coherencia en la db
@Service
@Transactional
public class UserService {
    @Autowired
    UserRepository userRepository;

    public Optional<UserEntity> getByUserName(String username){
        return userRepository.findByUserName(username);
    }
    public boolean existByUserName(String userName){
        return userRepository.existsByUserName(userName);
    }
    public boolean existByEmail(String email){
        return userRepository.existsByEmail(email);
    }
    public void save(UserEntity userEntity){
        userRepository.save(userEntity);
    }
}
