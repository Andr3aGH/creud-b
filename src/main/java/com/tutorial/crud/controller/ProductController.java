package com.tutorial.crud.controller;

import com.tutorial.crud.dto.Message;
import com.tutorial.crud.dto.ProductDTO;
import com.tutorial.crud.entity.Product;
import com.tutorial.crud.service.ProductService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/product")
@CrossOrigin(origins = "http://localhost:4200")
public class ProductController {
    @Autowired
    ProductService productService;

    @GetMapping("/list")
    public ResponseEntity<List<Product>> list(){
        List<Product> list = productService.list();
        return new ResponseEntity<List<Product>>(list, HttpStatus.OK);
    }
    @GetMapping("/detail/{id}")
    public ResponseEntity<Product> getById(@PathVariable("id") int id){
        if(!productService.existById(id))
            return new ResponseEntity(new Message("Doesn't Exist"), HttpStatus.NOT_FOUND);

        Product product = productService.getOne(id).get();
        return new ResponseEntity<Product>(product, HttpStatus.OK);
    }

    @GetMapping("/detailname/{name}")
    public ResponseEntity<Product> getByName(@PathVariable("name") String name){
        if(!productService.existByName(name))
            return new ResponseEntity(new Message("Doesn't Exist"), HttpStatus.NOT_FOUND);

        Product product = productService.getByName(name).get();
        return new ResponseEntity<Product>(product, HttpStatus.OK);
    }
    @PostMapping("/create")
    public ResponseEntity<?>  create(@RequestBody ProductDTO productDTO){
        if(StringUtils.isAllBlank(productDTO.getName()))
            return new ResponseEntity(new Message("The name is required "), HttpStatus.BAD_REQUEST);
        if( productDTO.getPrice() == null || productDTO.getPrice()<0 )
            return new ResponseEntity(new Message("The price is required "), HttpStatus.BAD_REQUEST);
        if (productService.existByName(productDTO.getName()))
            return new ResponseEntity(new Message("That name already exists"), HttpStatus.BAD_REQUEST);
        Product product = new Product(productDTO.getName(),productDTO.getPrice());
        productService.save(product);
        return new ResponseEntity(new Message("The product was created with success!"), HttpStatus.OK);
    }
    @PutMapping("/update/{id}")
    public ResponseEntity<?>  update (@PathVariable("id")int id, @RequestBody ProductDTO productDTO){
        if(!productService.existById(id))
            return new ResponseEntity(new Message("Doesn't Exist"), HttpStatus.NOT_FOUND);
        if(productService.existByName(productDTO.getName()) && productService.getByName (productDTO.getName()).get().getId() != id)
            return new ResponseEntity(new Message("That name already exists"), HttpStatus.BAD_REQUEST);
        if(StringUtils.isAllBlank(productDTO.getName()))
            return new ResponseEntity(new Message("The name is required "), HttpStatus.BAD_REQUEST);
        if(productDTO.getPrice() == null || productDTO.getPrice()<0 )
            return new ResponseEntity(new Message("The price is required "), HttpStatus.BAD_REQUEST);


        Product product = productService.getOne(id).get();
        product.setName(productDTO.getName());
        product.setPrice(productDTO.getPrice());
        productService.save(product);
        return new ResponseEntity(new Message("Upgraded product!"), HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") int id){
        if (!productService.existById(id))
            return  new ResponseEntity(new Message("Doesn't Exist"),HttpStatus.NOT_FOUND);
        productService.delete(id);
        return  new ResponseEntity(new Message("Item Removed"), HttpStatus.OK);
    }
}
